from main import convert

def test1():
    assert convert(1.0, 'm', 'mm') == 1000

def test2():
    assert convert(1.0, 'm', 'sm') == 100

def test3():
    assert convert(1.0, 'm', 'dm') == 10

def test4():
    assert convert(1.0, 'm', 'km') == 0.001

def test5():
    assert convert(1.0, 'mm', 'm') == 0.001

def test6():
    assert convert(1.0, 'mm', 'sm') == 0.1

def test7():
    assert convert(1.0, 'mm', 'dm') == 0.01

def test8():
    assert convert(1.0, 'mm', 'km') == 0.000001
from main import convert

def test9():
    assert convert(1.0, 'sm', 'mm') == 10


def test22():
    assert convert(2.0, 'sm', 'dm') == 0.2


def test11():
    assert convert(1.0, 'sm', 'm') == 0.01

def test12():
    assert convert(1.0, 'sm', 'km') == 0.00001

def test13():
    assert convert(1.0, 'dm', 'mm') == 100

def test14():
    assert convert(1.0, 'dm', 'sm') == 10

def test15():
    assert convert(1.0, 'dm', 'm') == 0.1

def test16():
    assert convert(1.0, 'dm', 'km') == 0.0001

def test17():
    assert convert(1.0, 'km', 'mm') == 1000000

def test18():
    assert convert(1.0, 'km', 'sm') == 100000

def test19():
    assert convert(1.0, 'km', 'm') == 1000

def test20():
    assert convert(1.0, 'km', 'dm') == 10000
